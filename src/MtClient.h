/*
 * MqTtClient.h
 *
 *  Created on: May 9, 2019
 *      Author: theo
 */

#ifndef HUMRELAY_MTCLIENT_H_
#define HUMRELAY_MTCLIENT_H_


// +++++++++++++ MQTT
// see http://nilhcem.com/iot/home-monitoring-with-mqtt-influxdb-grafana
// https://github.com/Nilhcem/home-monitoring-grafana/blob/master/03-bme280_mqtt/esp8266/esp8266.ino

#include <PubSubClient.h>
//esp32#include <WiFiClient.h>
#include <WiFi.h>
#include "DebugUtils.h"




class MtClient {
	const int  mqtt_port ;
public:
	MtClient(
			WiFiClient &pespClient,
			 const char * server,
			 const int   port,
			 const char * user,
			 const char * password,
			 const char * clientid,
			 const char * topic_state
	):mqtt_port(port){
		mqtt_server = server;
		// mqtt_port = port;
		mqtt_user = user ;
		mqtt_password = password;
		mqtt_clientid = clientid;
		mqtt_topic_state = topic_state;
		;
		espClient = pespClient ;
		mqttClient = new PubSubClient(espClient);

	};
	void mqttReconnect( unsigned long now) ;
	void mqttPublish(const char *topic, float payload) ;
	void mqttSetup(unsigned long now);
	void mqttLoop(unsigned long now);
    PubSubClient * mqttClient;


protected:
	const char * mqtt_server;
	const char * mqtt_user;
	const char * mqtt_password;
	const char * mqtt_clientid;
	const char * mqtt_topic_state ;

	WiFiClient  espClient;

	long lastMsgTime = 0;
    int mqttRecoDelay = 5000;
	unsigned long mqttRecoTimer = mqttRecoDelay ;       // to force first run without waiting delay


};

#endif /* HUMRELAY_MTCLIENT_H_ */
