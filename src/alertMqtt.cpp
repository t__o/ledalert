#include <Arduino.h>

/** Alert MQTT
 *
 * v02  24 sept 2019
 *
 * An ESP32 lights some ws2812 LEDs based on some MQTT messages.
 *
 * 2 modes are possible :
 *
 * Listen mode : listen for an mqtt message every delay
 *   use this mode to check if your existing mqtt devices are UP
 *
 * Direct mode : no timeout, led color is based on mqtt value
 *   use this mode for mqtt topic with an on/off state (ping)
 *
 *
 * Todo:
 * - mqtt is off
 * - Led animation
 *
 *
 * */

/// Debug
#define DEBUGLEVEL 2

#include "DebugUtils.h"
#include <FastLED.h>
#include "WiFi.h"
#include "MtClient.h"
#include "secret.h"

/// FastLED
#define LED_PIN     16
#define NUM_LEDS    20
#define CHIPSET     WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

/// timer to check for timeout
int resetLoopdelay = 100 * 60;      // limited by the ping duration


/// Wifi

void setupWifi() {

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
    }
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

/// MQTT

const char *MQTT_CLIENT_ID = "espAlert";
const int MQTT_PORT = 1883;
const char *MQTT_TOPIC_STATE = "home/alert/status";


WiFiClient espClient;

MtClient Mq(
        espClient,
        MQTT_SERVER,
        MQTT_PORT,
        MQTT_USER,
        MQTT_PASSWORD,
        MQTT_CLIENT_ID,
        MQTT_TOPIC_STATE
);


/// LED / MQTT Item

/** list of color mapping actions
 *
 * Most action set color if message and red if timeout
 * coloMQ actions do not use timeout
 *
 * */
enum colorAction {
    coloPing,                   // Green if message , red if timeout
    coloTempOut,                // Outside temp gradiant  from 0 to 25
    coloTempIn,                 // Inside temp gradiant  from 20 to 30
    colo10,                     // Gradient from 1 to 10
    coloMQ01,                   // Direct mqtt, no timeout, 0 red 1 green
};

struct MqItem {
    byte ledid;                 // LED number
    const char *topic;          // MQTT topic
    colorAction action;         // Action
    int delay;                  // Delay to consider of

    unsigned long timestamp;    // timestamp of last mqtt message
    String value;               // mqtt value
    byte color;                 // Calculated color based on action
    byte saturation;

    byte brightness;
};

const byte itemCount = 10;
MqItem items[itemCount] = {
        {1,  "home/bme280/temperature", coloPing,    30 * 1000 + 5},          // esp box
        {2,  "home/weather/temp",       coloPing,    1 * 60 * 1000 + 5},      // api weather
        {3,  "home/garden/temperature", coloPing,    2 * 60 * 1000 + 5},      // esp fenetre
        {4,  "home/meteo/temperature",  coloPing,    5 * 60 * 1000 + 5},      // esp room

        {5,  "home/ping/game",          coloMQ01,    1 * 60 * 1000 + 5},      // pc game
        {6,  "home/ping/espbox",        coloMQ01,    1 * 60 * 1000 + 5},      // esp box
        {7,  "home/ping/androcam",      coloMQ01,    1 * 60 * 1000 + 5},      // cam android box
        {8,  "home/ping/espcam",        coloMQ01,    1 * 60 * 1000 + 5},      // cam esp PH

        {9,  "home/weather/temp",       coloTempOut, 1 * 60 * 1000 + 5},
        {10, "home/bme280/temperature", coloTempIn,  30 * 1000 + 5}
};

/** If no message reset the LED (on timer) */
void resetColor(MqItem item) {
    if (millis() - item.timestamp > item.delay) {
        if (item.action == coloMQ01) item.color = HUE_ORANGE;                       // Orange if mqtt down
        item.saturation = 255;
        item.brightness = 255;
        leds[item.ledid] = CHSV(item.color, item.saturation, item.brightness);

        if (DEBUGLEVEL > 1) Serial.println(" Reset " + String(item.topic));
    }
}

/** Calc color (on incoming message)
 * https://github.com/FastLED/FastLED/wiki/Pixel-reference
 * Rainbow hue chart
 * HUE_RED = 0
 *
 * */
void calcColor(MqItem item) {

    item.saturation = 255;
    item.brightness = 200;

    if (item.action == coloPing) {
        item.color = HUE_GREEN;     // green
        //item.brightness = 50;
    } else if (item.action == coloMQ01) {
        // binary 85 green  0 red
        item.color = item.value.toInt() == 0 ? HUE_RED : HUE_GREEN;
    } else if (item.action == coloTempOut) {
        item.color = HUE_BLUE - map(item.value.toInt(), 0, 25, HUE_RED, HUE_BLUE);
        //item.brightness = 200;
    } else if (item.action == coloTempIn) {
        item.color = HUE_BLUE - map(item.value.toInt(), 20, 30, HUE_RED, HUE_BLUE);      // Box value
        //item.brightness = 200;
    } else if (item.action == colo10) {
        item.color = HUE_GREEN;
    }


    leds[item.ledid] = CHSV(item.color, item.saturation, item.brightness);
}

/** Callback update String Value and Timestamp of leds */
void callback(char *topic, byte *payload, unsigned int length) {
    // Check matching topic
    for (int i = 0; i < itemCount; ++i) {
        if (String(topic) == String(items[i].topic)) {
            String text;
            for (int j = 0; j < length; j++) {
                text = text + (char) payload[j];
            }
            bool firstRun = items[i].timestamp == 0;

            if (DEBUGLEVEL > 1) {

                Serial.println(" msg " + String(items[i].topic) + " " + String(text));


            }


            // record timestamp and String value
            items[i].timestamp = millis();
            items[i].value = String(text);

            // On first message : do not color leds
            if (firstRun)
                continue;

            // update item just after mqtt message
            calcColor(items[i]);
            FastLED.show();
        }
    }
}

void setupMqtt() {
    Mq.mqttSetup(millis());
    Mq.mqttClient->setCallback(callback);                       // callbacks
    for (int i = 0; i < itemCount; ++i) {                       // subscribe all topics
        Mq.mqttClient->subscribe(items[i].topic);
    }
}

void clearLeds() {
    for (int j = 0; j < NUM_LEDS; j++) {
        leds[j] = CHSV(0, 0, 0);
    }
    FastLED.show();
}


/// Setup
unsigned long now;
unsigned long resetLoopTimer;

void setup() {
    delay(1000);
    Serial.begin(115200);

    FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalSMD5050);
    FastLED.setBrightness(250);
    clearLeds();

    setupWifi();
    setupMqtt();
}

/** By defaut fastled use Rainbow hue chart
 *    use this method to validate colors
 * */
void checkColors() {
    leds[0] = CHSV(0, 255, 255);
    leds[1] = CHSV(96, 255, 255);
    leds[2] = CHSV(96, 150, 255);
    leds[3] = CHSV(96, 100, 255);

    leds[4] = CHSV(96, 50, 255);
    leds[5] = CHSV(96, 20, 255);
    leds[6] = CHSV(96, 255, 200);
    leds[7] = CHSV(96, 255, 150);

    leds[8] = CRGB(96, 255, 255);
    leds[9] = CHSV(96, 255, 255);
    leds[10] = CHSV(32, 255, 255);
    leds[11] = CHSV(42, 255, 255);
    leds[12] = CHSV(64, 255, 255);

    leds[13] = CRGB(171, 255, 255);
    leds[14] = CHSV(171, 255, 255);

    FastLED.show();
    FastLED.delay(20);
}

void loop() {
    now = millis();
    Mq.mqttLoop(now);

    //checkColors();    return;

    // Led Timer
    if (now - resetLoopTimer > resetLoopdelay) {

        // check if message timer has expired ?
        for (int i = 0; i < itemCount; ++i) {
            resetColor(items[i]);
        }
        FastLED.show();

        resetLoopTimer = now;
    }
}