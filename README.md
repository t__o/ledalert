
# Alert Mqtt Box


## Goals

Use a WS2812 LED strip to display status from MQTT messages.


### Examples  
- ping an host (server side logic)
- check if an MQTT topic is emitting
- temperature indicator

Todo :
- next metro delay 
- bitcoin price evolution

[Home exemple](./LedAlert.jpeg)

The 2 left LEDs are for inside and outside temperature,  
others are ping or mqtt check


### Logic

Every LED is configured with  : 
- led id        
- mqtt topic to listen
- delay of the mqtt topic       
- function to color the led (bool, gradient)       

The script listen for the MQTT topic and then apply the coloring function.  
If no message is received after the delay, the led is set off.


Some LED/alert can use already existing MQTT topic like :
- check if it emits
- display a gradient based on value (temperature)

Other alerts, like a ping require external scripts to feed MQTT server


### Usage

- Rename Notsecret.h to secret.h and update with you Wifi and MQTT secrets.
- Configure your LEDs in MqItem items array

### Server 

Some MQTT client script to ping some device, or call a weather API.  
You can run them on your MQTT server.

You can find a good example of MQTT IoT Monitoring here : https://github.com/Nilhcem/home-monitoring-grafana