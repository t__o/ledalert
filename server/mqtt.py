import paho.mqtt.client as mqtt


def mqtt_connect(
        mqclientid,
        conf,
        on_connect=False,
        on_message=False
):
    mqtt_client = mqtt.Client(mqclientid)
    mqtt_client.username_pw_set(conf['mquser'], conf['mqpassword'])
    if (on_connect): mqtt_client.on_connect = on_connect
    if (on_message): mqtt_client.on_message = on_message
    mqtt_client.connect(conf['mqaddress'], 1883)
    return mqtt_client


def mqtt_publish(mqtt_client, topic, value):
    print('publish ' + topic + ' ' + str(value))
    mqtt_client.publish(topic, value)
