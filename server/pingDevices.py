import os
from secretsConf import sec as config
from mqtt import mqtt_connect
from mqtt import mqtt_publish

### Config
network = '192.168.1.'
ips = {
    'game': 10,
    'espbox': 18,
    'androcam': 21,
    'espcam': 17
}
### Config


def ping(hostname):
    # hostname = "google.com" #example
    response = os.system("ping -c 1 " + hostname)
    # and then check the response...
    if response == 0:
        return 1
    else:
        return 0


print('Ping Devices')

# connect mqtt
mqtt_client = mqtt_connect('pingDevices', config)

for name, ip in ips.items():
    res = ping(network + str(ip))
    mqtt_publish(mqtt_client, 'home/ping/' + name, int(res))
