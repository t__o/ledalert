import requests
import json
import time

#import os, sys;
#sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from secretsConf import sec as config
from mqtt import mqtt_connect
from mqtt import mqtt_publish

weatherConfig = {
    'apiCity': 'Paris,fr',
    'cityId': '2988507'
}
apiUrl = 'https://api.openweathermap.org/data/2.5/weather?' + \
         'id=' + weatherConfig['cityId'] + \
         '&units=metric' + \
         '&appid=' + config['weatherApikey']


def getApiWeather():
    resp = requests.get(apiUrl)
    if resp.status_code != 200:
        # This means something went wrong.
        raise ApiError('GET /tasks/ {}'.format(resp.status_code))
    return resp.json()


def parseApiData(data):
    return {
        'temp': data['main']['temp'],
        'hum': data['main']['humidity'],
        # TODO

    }


def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)


# fetch api data
apiResp = getApiWeather()
# jprint(apiResp)

# parse data
data = parseApiData(apiResp)

# connect mqtt
mqtt_client = mqtt_connect('getWeather', config)

# send messages
for key, value in data.items():
    mqtt_publish(mqtt_client, 'home/weather/' + key, value)
